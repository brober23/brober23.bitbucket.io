var searchData=
[
  ['omega_5fmeas_0',['Omega_meas',['../class_lab0x04__closed_loop_1_1_closed_loop.html#a29c85179b4710275c2a3f6e6af9eee37',1,'Lab0x04_closedLoop.ClosedLoop.Omega_meas()'],['../class_lab0x05__closed_loop_1_1_closed_loop.html#ad66825ad89c7fd01043a84c40f0cc100',1,'Lab0x05_closedLoop.ClosedLoop.Omega_meas()']]],
  ['omega_5fref_1',['Omega_ref',['../class_lab0x04__closed_loop_1_1_closed_loop.html#a1d93de3b02ba51aaa928ad976f225dca',1,'Lab0x04_closedLoop.ClosedLoop.Omega_ref()'],['../class_lab0x05__closed_loop_1_1_closed_loop.html#a675bad0ad1fb4bedf9e48506eecc0ce6',1,'Lab0x05_closedLoop.ClosedLoop.Omega_ref()']]],
  ['omega_5fx_2',['omega_x',['../class_b_n_o055_1_1_b_n_o055.html#a2b97b1a72257c19e211356e3b289b418',1,'BNO055::BNO055']]],
  ['omega_5fy_3',['omega_y',['../class_b_n_o055_1_1_b_n_o055.html#aba6f37685f7ff009708c148681a429e1',1,'BNO055::BNO055']]],
  ['omega_5fz_4',['omega_z',['../class_b_n_o055_1_1_b_n_o055.html#a212fd98f74bb79dc3093c752a8a5f99d',1,'BNO055::BNO055']]]
];
